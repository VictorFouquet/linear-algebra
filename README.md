# Linear Algebra

Basic library including matrices and vector class implementations with their basic operation.

## Build the project

### Prerequesite

Cmake ^3.15

### Procedure

Create a build repository at the root folder of the project, open a terminal inside that build folder, and run

```shell
cmake .. && make
```

Run the example from the build repository with

```shell
./LinearAlgebraExample
```