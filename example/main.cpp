#include "vector3d.h"
#include "matrix.h"
#include <iostream>
#include <stdlib.h>

int main()
{
    Vector3d a = Vector3d(-1.0f, -1.0f, 0.0f);
    Vector3d b = Vector3d( 1.0f, -1.0f, 0.0f);
    Vector3d c = Vector3d( 1.0f,  1.0f, 0.0f);
    Vector3d d = Vector3d(-1.0f,  1.0f, 0.0f);


    Matrix mat = Matrix::identity();

    Matrix translateMat;
    Matrix scaleMat;
    Matrix rotateMat;
    Matrix transform;

    std::cout << "Initialize vectors: " << std::endl;
    a.print();
    b.print();
    c.print();
    d.print();

    std::cout << "\nCreate Identity Matrix: " << std::endl;
    mat.print();

    std::cout << "Multiply vector a by identity matrix: " << std::endl;
    Vector3d id = mat * a;
    id.print();

    std::cout << "\nScale all vectors by 2 on x and y axis: " << std::endl;
    scaleMat = Matrix::scale(2.f, 2.f, 1.f);

    (scaleMat * a).print();
    (scaleMat * b).print();
    (scaleMat * c).print();
    (scaleMat * d).print();

    std::cout << "\nTranslate all vectors by +1 unit on x and -2 unit on y: " << std::endl;
    translateMat = Matrix::translate(1.f, -2.f, 0.f);

    (translateMat * a).print();
    (translateMat * b).print();
    (translateMat * c).print();
    (translateMat * d).print();

    std::cout << "\nScale by 2 and translate all vectors by +1 unit on x and -2 unit on y: " << std::endl;
    (translateMat * scaleMat * a).print();
    (translateMat * scaleMat * b).print();
    (translateMat * scaleMat * c).print();
    (translateMat * scaleMat * d).print();

    std::cout << "\nApply the same as previous using transform matrix: " << std::endl;
    transform = translateMat * scaleMat;
    (transform * a).print();
    (transform * b).print();
    (transform * c).print();
    (transform * d).print();

    std::cout << "\nRotate all vectors by 90° on x axis" << std::endl;
    rotateMat = Matrix::rotate(90.f, 0.f, 0.f);
    (rotateMat * a).print();
    (rotateMat * b).print();
    (rotateMat * c).print();
    (rotateMat * d).print();

    std::cout << "\nRotate all vectors by 90° on y axis" << std::endl;
    rotateMat = Matrix::rotate(0.f, 90.f, 0.f);
    (rotateMat * a).print();
    (rotateMat * b).print();
    (rotateMat * c).print();
    (rotateMat * d).print();

    std::cout << "\nRotate all vectors by 90° on z axis" << std::endl;
    rotateMat = Matrix::rotate(0.f, 0.f, 90.f);
    (rotateMat * a).print();
    (rotateMat * b).print();
    (rotateMat * c).print();
    (rotateMat * d).print();

    std::cout << "\nRotate all vectors by 90° on x axis and y" << std::endl;
    rotateMat = Matrix::rotate(90.f, 90.f, 0.f);
    (rotateMat * a).print();
    (rotateMat * b).print();
    (rotateMat * c).print();
    (rotateMat * d).print();

    std::cout << "\nRotate all vectors by 90° on x axis and z" << std::endl;
    rotateMat = Matrix::rotate(90.f, 0.f, 90.f);
    (rotateMat * a).print();
    (rotateMat * b).print();
    (rotateMat * c).print();
    (rotateMat * d).print();

    std::cout << "\nRotate all vectors by 90° on y axis and z" << std::endl;
    rotateMat = Matrix::rotate(0.f, 90.f, 90.f);
    (rotateMat * a).print();
    (rotateMat * b).print();
    (rotateMat * c).print();
    (rotateMat * d).print();

    std::cout << "\nRotate all vectors by 90° on all axis" << std::endl;
    rotateMat = Matrix::rotate(90.f, 90.f, 90.f);
    (rotateMat * a).print();
    (rotateMat * b).print();
    (rotateMat * c).print();
    (rotateMat * d).print();

    std::cout << "\nRotate all vectors by 90° on z axis and scale by 2" << std::endl;
    rotateMat = Matrix::rotate(0.f, 0.f, 90.f);
    transform = rotateMat * scaleMat;
    (transform * a).print();
    (transform * b).print();
    (transform * c).print();
    (transform * d).print();

    std::cout << "\nTransform with translate scale and rotate" << std::endl;
    translateMat = Matrix::translate(-1.f, 0.f, 0.f);
    transform =  translateMat * scaleMat * rotateMat;
    (transform * a).print();
    (transform * b).print();
    (transform * c).print();
    (transform * d).print();

    std::cout << "\nOrthographic Projection" << std::endl;
    // Creates scene space coordinates, a 2 unit depth box,
    // with bottom left front plane point located at { 5, 5, -1 } and a 16/9 ratio; 
    float xOrigin = 5.f, yOrigin = 5.f, width = 16.f, height = 9.f, near = -1.f, far = 1.f;

    // Computes center point coordinates of the box.    
    Vector3d boxCenter = Vector3d(xOrigin + width / 2, yOrigin + height / 2, (far + near) / 2);
    
    // Computes orthographic projection transformation matrix.
    Matrix orth = Matrix::orthographicProj(xOrigin, xOrigin + width, yOrigin, yOrigin + height, near, far);

    // Canonical space is defined by a box of 2 units width, height and depth.
    // The center of its front face is located at the absolute origin.
    // Its center is then located at { 0, 0, 1 };
    // Box center when transformed by orthographic projection should be at the canonical space's origin.
    (orth * boxCenter).print();

    Vector3d p1 = orth * Vector3d(xOrigin + 4, yOrigin + 3, 0.f);
    Vector3d p2 = orth * Vector3d(xOrigin + 8, yOrigin + 3, 0.f);
    Vector3d p3 = orth * Vector3d(xOrigin + 8, yOrigin + 7, 0.f);
    Vector3d p4 = orth * Vector3d(xOrigin + 4, yOrigin + 7, 0.f);
    p1.print();
    p2.print();
    p3.print();
    p4.print();
    
    // Converts points to a viewport twice the size of scene's width and height
    std::cout << "Viewport" << std::endl;
    
    Matrix viewportTransform = Matrix::viewportTransform(64.f, 36.f);

    (viewportTransform * p1).print();
    (viewportTransform * p2).print();
    (viewportTransform * p3).print();
    (viewportTransform * p4).print();
}
